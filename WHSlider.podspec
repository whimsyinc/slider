Pod::Spec.new do |s|

  s.name         = "WHSlider"
  s.version      = "0.0.1"
  s.summary      = "A short description of WHSlider."

  s.description  = <<-DESC
                   A longer description of WHSlider in Markdown format.

                   * Think: Why did you write this? What is the focus? What does it do?
                   * CocoaPods will be using this to generate tags, and improve search results.
                   * Try to keep it short, snappy and to the point.
                   * Finally, don't worry about the indent, CocoaPods strips it!
                   DESC

  s.homepage     = "http://bitbucket.org/whimsyinc/slider"

  s.license      = 'MIT (example)'

  s.author             = { "James Dumay" => "james.w.dumay@gmail.com" }

  s.platform     = :osx
  s.source       = { :git => "git@bitbucket.org:whimsyinc/slider.git" }

  s.source_files  = 'Slider/NSObject+PerformBlock*.{h,m}', 'Slider/WHSlideView*.{h,m}'

  s.public_header_files = 'Slider/WHSlideView*.h'
  s.requires_arc = true
  # s.dependency 'JSONKit', '~> 1.4'

end
