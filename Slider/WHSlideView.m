//
//  WHSlideView.m
//  Slider
//
//  Created by James Dumay on 28/01/2014.
//  Copyright (c) 2014 Whimsy. All rights reserved.
//


#import <QuartzCore/QuartzCore.h>

#import "WHSlideView.h"
#import "NSObject+PerformBlock.h"

#define kSlideSwipeAnimationDuration    0.3
#define kClickableAreaWidth             100

@interface WHSlideView()

@property (assign, nonatomic) NSUInteger visibleIndex;
@property (retain) NSMutableDictionary *twoFingersTouches;
@property (strong) NSTrackingArea *trackingArea;

@end

@implementation WHSlideView

- (id)initWithFrame:(NSRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.wantsLayer = YES;
        self.acceptsTouchEvents = YES;
        self.postsFrameChangedNotifications = YES;
    }
    return self;
}

-(void)awakeFromNib
{
    [super awakeFromNib];
    
    [self.window setAcceptsMouseMovedEvents:YES];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(recalulateLayout:) name:NSViewFrameDidChangeNotification object:self];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(recalulateLayout:) name:NSWindowDidEnterFullScreenNotification object:self.window];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(recalulateLayout:) name:NSWindowDidExitFullScreenNotification object:self.window];
    
    [self.window makeFirstResponder:self];
}

-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:NSViewFrameDidChangeNotification object:self];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:NSWindowDidEnterFullScreenNotification object:self.window];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:NSWindowDidExitFullScreenNotification object:self.window];
}

#pragma mark - Public interface

-(void)setDelegate:(id<WHSlideViewDelegate>)delegate
{
    if (_delegate)
    {
        [_layer removeAllAnimations];
        
        NSArray *sublayers = [NSArray arrayWithArray:_layer.sublayers];
        for (CALayer *layer in sublayers)
        {
            [layer removeFromSuperlayer];
        }
    }
    
    _delegate = delegate;
    
    self.visibleIndex = 0;
    if ([_delegate respondsToSelector:@selector(startingIndexForSlideView:)])
    {
        self.visibleIndex = [self.delegate startingIndexForSlideView:self];
    }
    
    [self prepareLayers];
}

-(void)slideLeft
{
    if ([self canMoveLeft])
    {
        [CATransaction begin];
        
        CFTimeInterval beginTime = CACurrentMediaTime();
        [self moveLayer:self.visibleLayer to:self.rightPosition  duration:kSlideSwipeAnimationDuration beginTime:beginTime];
        [self moveLayer:self.leftLayer to:self.visibleLayerPosition duration:kSlideSwipeAnimationDuration beginTime:beginTime];
        
        [CATransaction commit];
        
        [self didHideLayer:self.visibleLayer];
        
        [self deincrementVisibleIndex];
        [self setVisibleLayer:self.leftLayer];
        
        _leftLayer = nil;
        [self prepareInvisibleLayers];
    }
    else
    {
        NSBeep();
    }
}

-(void)slideRight
{
    if ([self canMoveRight])
    {
        [CATransaction begin];
        
        CFTimeInterval beginTime = CACurrentMediaTime();
        [self moveLayer:self.visibleLayer to:self.leftPosition  duration:kSlideSwipeAnimationDuration beginTime:beginTime];
        [self moveLayer:self.rightLayer to:self.visibleLayerPosition duration:kSlideSwipeAnimationDuration beginTime:beginTime];
        
        [CATransaction commit];
        
        [self didHideLayer:self.visibleLayer];
        [self incrementVisibleIndex];
        [self setVisibleLayer:self.rightLayer];
        
        _rightLayer = nil;

        [self prepareInvisibleLayers];
    }
    else
    {
        NSBeep();
    }
}

-(void)reload
{
    [self prepareLayers];
}

#pragma mark - Layers

-(CALayer*)newLayerForIndex:(NSUInteger)index
{
    if ([_delegate respondsToSelector:@selector(slideView:newLayerForIndex:)])
    {
        CALayer *layer = [_delegate slideView:self newLayerForIndex:index];
        if (layer)
        {
            return layer;
        }
    }
    return [CALayer layer];
}

-(void)prepareInvisibleLayers
{
    NSUInteger previousIndex = _visibleIndex - 1;
    if ([self canMoveLeft])
    {
        if (self.leftLayer)
        {
            [self.leftLayer removeFromSuperlayer];
        }
        _leftLayer = [self prepareLayer:self.leftLayer index:previousIndex];
        [self doLayout:self.leftLayer];
        
        [CATransaction begin];
        [CATransaction setValue:(id)kCFBooleanTrue forKey:kCATransactionDisableActions];
        
        self.leftLayer.position = [self leftPosition];
        
        [CATransaction commit];
        
        [_layer addSublayer:self.leftLayer];
    }
    
    NSUInteger nextIndex = _visibleIndex + 1;
    if ([self canMoveRight])
    {
        if (self.rightLayer)
        {
            [self.rightLayer removeFromSuperlayer];
        }
        _rightLayer = [self prepareLayer:self.rightLayer index:nextIndex];
        [self doLayout:self.rightLayer];
        
        [CATransaction begin];
        [CATransaction setValue:(id)kCFBooleanTrue forKey:kCATransactionDisableActions];
        
        self.rightLayer.position = [self rightPosition];
        
        [CATransaction commit];
        
        [_layer addSublayer:self.rightLayer];
    }
}

-(void)setVisibleLayer:(CALayer *)visibleLayer
{
    _visibleLayer = visibleLayer;
    [self.layer addSublayer:visibleLayer];
    
    if ([_delegate respondsToSelector:@selector(slideView:didShowLayer:atIndex:)])
    {
        [_delegate slideView:self didShowLayer:self.visibleLayer atIndex:self.visibleIndex];
    }
}

-(void)prepareLayers
{
    [self prepareInvisibleLayers];
    CALayer *layer = [self prepareLayer:self.visibleLayer index:_visibleIndex];
    [self setVisibleLayer:layer];
}

-(CALayer *)prepareLayer:(CALayer*)layer index:(NSUInteger)index
{
    //Lay out the layer
    layer = [self newLayerForIndex:index];
    
    //Prepare contents of the layer
    [self performBlockInBackground:^{
        [_delegate slideView:self prepareLayer:layer index:index];
    }];
    
    //Lay it out
    [self doLayout:layer];
    
    return layer;
}

-(void)didHideLayer:(CALayer*)layer
{
    if ([_delegate respondsToSelector:@selector(slideView:didHideLayer:atIndex:)] && layer)
    {
        [_delegate slideView:self didHideLayer:layer atIndex:self.visibleIndex];
    }
}

-(NSUInteger)incrementVisibleIndex
{
    if ((_visibleIndex + 1) < [_delegate numberOfImagesForSlideView:self])
    {
        return _visibleIndex++;
    }
    return _visibleIndex;
}

-(NSUInteger)deincrementVisibleIndex
{
    if ((_visibleIndex + 1) > 0)
    {
        return _visibleIndex--;
    }
    return _visibleIndex;
}

#pragma mark - Drawing and Layout

//Need to draw the same bg color as visible layer for scaling on resize events
-(void)drawRect:(NSRect)dirtyRect
{
    CGColorRef backgroundColor = self.visibleLayer.backgroundColor;
    if (backgroundColor == NULL)
    {
        [super drawRect:dirtyRect];
    }
    else
    {
        CGContextRef ctx = (CGContextRef) [[NSGraphicsContext currentContext] graphicsPort];
        CGContextSetFillColorWithColor(ctx, backgroundColor);
        CGContextFillRect(ctx, dirtyRect);
    }
}

-(void)moveLayer:(CALayer*)layer to:(CGPoint)point duration:(CFTimeInterval)duration beginTime:(CFTimeInterval)beginTime
{
    CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"position"];
    animation.fromValue = [layer valueForKey:@"position"];
    animation.toValue = [NSValue valueWithPoint:point];
    animation.duration = duration;
    animation.beginTime = beginTime;
    layer.position = point;
    [layer addAnimation:animation forKey:@"position"];
}

-(void)updateFrame:(CALayer*)layer to:(CGRect)frame duration:(CFTimeInterval)duration beginTime:(CFTimeInterval)beginTime
{
    CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"frame"];
    animation.fromValue = [layer valueForKey:@"frame"];
    animation.toValue = [NSValue valueWithRect:frame];
    animation.duration = duration;
    animation.beginTime = beginTime;
    layer.frame = frame;
    [layer addAnimation:animation forKey:@"frame"];
}

-(CGPoint)visibleLayerPosition
{
    return CGPointMake(self.bounds.size.width / 2, self.bounds.size.height / 2);
}

-(CGPoint)leftPosition
{
    CGFloat padding = [self paddingForSlideViewLayers];
    CGFloat width = self.frame.size.width;
    return CGPointMake((self.visibleLayerPosition.x + width + padding) * -1, self.visibleLayerPosition.y);
}

-(CGPoint)rightPosition
{
    CGFloat padding = [self paddingForSlideViewLayers];
    CGFloat width = self.frame.size.width;
    return CGPointMake((self.visibleLayerPosition.x + width + padding), self.visibleLayerPosition.y);
}

-(void)recalulateLayout:(NSNotification*)notification
{
    //Bring layer to front
    [self.layer addSublayer:self.visibleLayer];
    
    [CATransaction begin];
    
    CFTimeInterval beginTime = CACurrentMediaTime();
    
    [self updateFrame:self.visibleLayer to:self.bounds duration:kSlideSwipeAnimationDuration beginTime:beginTime];
    [self updateFrame:self.leftLayer to:self.bounds duration:kSlideSwipeAnimationDuration beginTime:beginTime];
    [self updateFrame:self.rightLayer to:self.bounds duration:kSlideSwipeAnimationDuration beginTime:beginTime];
    
    [self doLayout:self.rightLayer];
    [self doLayout:self.leftLayer];
    [self doLayout:self.visibleLayer];
    
    [CATransaction commit];
    
    [self updateTrackingAreas];
}

-(void)doLayout:(CALayer*)layer
{
    if (!CGRectEqualToRect(self.bounds, layer.frame))
    {
        [layer setFrame:self.bounds];
    }
    
    [layer layoutSublayers];
    
    if ([_delegate respondsToSelector:@selector(slideView:layoutChangedForLayer:)])
    {
        [_delegate slideView:self layoutChangedForLayer:layer];
    }
}

#pragma mark - Input

-(BOOL)acceptsFirstResponder
{
    return YES;
}

-(BOOL)wantsScrollEventsForSwipeTrackingOnAxis:(NSEventGestureAxis)axis
{
    return axis == NSEventGestureAxisHorizontal;
}

-(void)scrollWheel:(NSEvent *)event
{
    if ([event phase] == NSEventPhaseNone) return; // Not a gesture scroll event.
    if (fabsf([event scrollingDeltaX]) <= fabsf([event scrollingDeltaY])) return; // Not horizontal
    if (![NSEvent isSwipeTrackingFromScrollEventsEnabled]) return;
    
    [event trackSwipeEventWithOptions:NSEventSwipeTrackingClampGestureAmount dampenAmountThresholdMin:-1 max:1 usingHandler:^(CGFloat gestureAmount, NSEventPhase phase, BOOL isComplete, BOOL *stop) {
       
        [CATransaction begin];
        
        CGFloat width = self.frame.size.width;
        CGFloat scaleFactor = fabsf((self.widthForScale * fabsf(gestureAmount)));
        CGFloat offset = scaleFactor - width;
        
        CGFloat padding = [self paddingForSlideViewLayers];
        
        //Swipe from right
        if (gestureAmount <= -0  && [self canMoveRight])
        {
            [self.leftLayer removeFromSuperlayer];
            [_layer addSublayer:self.rightLayer];
            
            CFTimeInterval beginTime = CACurrentMediaTime();
            [self moveLayer:self.visibleLayer to:CGPointMake((self.visibleLayerPosition.x - width) - offset, self.visibleLayerPosition.y) duration:kSlideSwipeAnimationDuration beginTime:beginTime];
            
            CGFloat rightX = (self.visibleLayerPosition.x + padding) - offset;
            
            if (rightX > self.visibleLayerPosition.x)
            {
                [self moveLayer:self.rightLayer to:CGPointMake(rightX, self.visibleLayerPosition.y) duration:kSlideSwipeAnimationDuration beginTime:beginTime];
            }
        }
        
        if (isComplete && gestureAmount == -1 && [self canMoveRight])
        {
            [self didHideLayer:self.visibleLayer];
            
            [self incrementVisibleIndex];
            [self setVisibleLayer:self.rightLayer];
            _rightLayer = nil;
            [self prepareInvisibleLayers];
        }
        
        //Swipe from left
        if (gestureAmount >= 0 && [self canMoveLeft])
        {
            [self.rightLayer removeFromSuperlayer];
            [_layer addSublayer:self.leftLayer];
            
            CFTimeInterval beginTime = CACurrentMediaTime();
            [self moveLayer:self.visibleLayer to:CGPointMake(self.rightPosition.x + offset, self.visibleLayerPosition.y) duration:kSlideSwipeAnimationDuration beginTime:beginTime];
            
            CGFloat leftX = self.visibleLayerPosition.x + offset;
            if (leftX < self.visibleLayerPosition.x)
            {
                [self moveLayer:self.leftLayer to:CGPointMake(leftX, self.visibleLayerPosition.y) duration:kSlideSwipeAnimationDuration beginTime:beginTime];
            }
        }
        
        if (isComplete && gestureAmount == 1 && [self canMoveLeft])
        {
            [self didHideLayer:self.visibleLayer];
            
            [self deincrementVisibleIndex];
            [self setVisibleLayer:self.leftLayer];
            _leftLayer = nil;
            
            [self prepareInvisibleLayers];
        }
        
        [CATransaction commit];
        
        if (isComplete)
        {
            [self moveLayer:self.visibleLayer to:self.visibleLayerPosition duration:0.1 beginTime:CACurrentMediaTime()];
        }
    }];
}

-(CGFloat)paddingForSlideViewLayers
{
    if ([_delegate respondsToSelector:@selector(paddingForSlideViewLayers:)])
    {
        return [_delegate paddingForSlideViewLayers:self];
    }
    return 0;
}

-(BOOL)canMoveLeft
{
    NSUInteger previousIndex = _visibleIndex - 1;
    return previousIndex != NSNotFound && [self.delegate numberOfImagesForSlideView:self] >= previousIndex;
}

-(BOOL)canMoveRight
{
    NSUInteger nextIndex = _visibleIndex + 1;
    return nextIndex < [self.delegate numberOfImagesForSlideView:self];
}

-(CGFloat)widthForScale
{
    return self.window.frame.size.width;
}

-(void)mouseDown:(NSEvent *)theEvent
{
    if (theEvent.clickCount == 1)
    {
        NSRect leftRect = NSMakeRect(self.frame.origin.x, self.frame.origin.y, kClickableAreaWidth, self.frame.size.height);
        
        NSPoint pointInView = [self convertPoint:[theEvent locationInWindow] fromView:nil];
        BOOL isLeftAction = NSPointInRect(pointInView, leftRect);
        if (isLeftAction)
        {
            [self slideLeft];
        };
        
        NSRect rightRect = NSMakeRect(self.frame.size.width - kClickableAreaWidth, self.frame.origin.y, kClickableAreaWidth, self.frame.size.height);
        BOOL isRightAction = NSPointInRect(pointInView, rightRect);
        if (isRightAction)
        {
            [self slideRight];
        };
        
        if (!isLeftAction && !isRightAction && [_delegate respondsToSelector:@selector(slideView:layerClicked:)])
        {
            NSPoint point = [self convertPointToLayer:theEvent.locationInWindow];
            if ([self.layer hitTest:point])
            {
                [_delegate slideView:self layerClicked:self.visibleLayer];
            }
        }
    }
}

-(void)mouseMoved:(NSEvent *)theEvent
{
    NSPoint point = [self convertPointToLayer:theEvent.locationInWindow];
    if ([self.layer hitTest:point] && [_delegate respondsToSelector:@selector(slideView:mouseMovedInLayer:point:)])
    {
        [_delegate slideView:self mouseMovedInLayer:self.visibleLayer point:point];
    }
}

- (void)keyDown:(NSEvent *)event
{
    NSString *characters = [event characters];
    
    unichar character = [characters characterAtIndex: 0];
    
    if (character == NSRightArrowFunctionKey)
    {
        [self slideRight];
    }
    else if (character == NSLeftArrowFunctionKey)
    {
        [self slideLeft];
    }
}

-(void)updateTrackingAreas
{
    [super updateTrackingAreas];
    
    if (_trackingArea)
    {
        [self removeTrackingArea:_trackingArea];
    }
    
    NSTrackingAreaOptions options = NSTrackingActiveInKeyWindow | NSTrackingMouseMoved;
    _trackingArea = [[NSTrackingArea alloc] initWithRect:self.bounds options:options owner:self userInfo:nil];
    [self addTrackingArea:_trackingArea];
}

@end