//
//  WHSlideView.h
//  Slider
//
//  Created by James Dumay on 28/01/2014.
//  Copyright (c) 2014 Whimsy. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "WHSlideViewDelegate.h"

@interface WHSlideView : NSView

@property (readonly, strong, nonatomic) CALayer *visibleLayer;
@property (readonly, strong) CALayer *leftLayer;
@property (readonly, strong) CALayer *rightLayer;

@property (weak, nonatomic) IBOutlet id<WHSlideViewDelegate> delegate;

-(void)slideLeft;

-(void)slideRight;

-(void)reload;

@end
