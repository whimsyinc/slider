//
//  WHAppDelegate.m
//  Slider
//
//  Created by James Dumay on 28/01/2014.
//  Copyright (c) 2014 Whimsy. All rights reserved.
//

#import "WHAppDelegate.h"
#import "NSImage+CGImage.h"
#import "NSObject+PerformBlock.h"

@implementation WHAppDelegate

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
    _items = [NSMutableArray array];
    
    NSURL *url = [NSURL fileURLWithPath:@"/Library/Desktop Pictures/"];
    
    NSDirectoryEnumerator *enumerator = [[NSFileManager defaultManager]
                                        enumeratorAtURL:url includingPropertiesForKeys:[NSArray array] options:NSDirectoryEnumerationSkipsSubdirectoryDescendants errorHandler:^BOOL(NSURL *url, NSError *error)
                                        {
                                            NSLog(@"Error listing '%@' album: %@", url, error.userInfo);
                                            return YES;
                                        }];
    for (NSURL *url in enumerator)
    {
        NSNumber *isDirectory = nil;
        [url getResourceValue:&isDirectory forKey:NSURLIsDirectoryKey error:nil];
        
        if (![isDirectory boolValue])
        {
            [_items addObject:url];
        }
    }
    
    _slideView.delegate = self;
}

- (IBAction)left:(id)sender
{
    [_slideView slideLeft];
}

- (IBAction)right:(id)sender
{
    [_slideView slideRight];
}

- (IBAction)reset:(id)sender
{
    _slideView.delegate = self;
}

- (IBAction)reload:(id)sender
{
    [_slideView reload];
}

-(NSUInteger)startingIndexForSlideView:(WHSlideView *)slideView
{
    return 8;
}

-(NSUInteger)numberOfImagesForSlideView:(WHSlideView *)slideView
{
    return _items.count;
}

-(CGFloat)paddingForSlideViewLayers:(WHSlideView *)slideView
{
    return 20;
}

-(void)slideView:(WHSlideView *)slideView didHideLayer:(CALayer *)layer
{
    NSLog(@"Layer hidden %@", layer.name);
}

-(void)slideView:(WHSlideView *)slideView didShowLayer:(CALayer *)layer
{
    NSLog(@"Layer shown %@", layer.name);
}

-(void)slideView:(WHSlideView *)slideView prepareLayer:(CALayer *)layer index:(NSUInteger)index
{
    NSURL *url = _items[index];
    NSLog(@"Index: %lu URL: %@", (unsigned long)index, url);
    [self performBlockOnMainThread:^{
        layer.contents = (__bridge id)thumbnail(url, self.window.frame.size);
        layer.name = [url lastPathComponent];
    }];
}

CGImageRef thumbnail(NSURL *url, CGSize size)
{
    CGImageRef imageRef = NULL;
    
    CGImageSourceRef imageSource = CGImageSourceCreateWithURL((__bridge CFURLRef)url, NULL);
    if (imageSource != nil)
    {
        CFDictionaryRef imageProperties = CGImageSourceCopyPropertiesAtIndex(imageSource, 0, NULL);
        if (imageProperties != nil)
        {
            CFNumberRef pixelWidthRef  = CFDictionaryGetValue(imageProperties, kCGImagePropertyPixelWidth);
            CFNumberRef pixelHeightRef = CFDictionaryGetValue(imageProperties, kCGImagePropertyPixelHeight);
            CGFloat pixelWidth = [(__bridge NSNumber *)pixelWidthRef floatValue];
            CGFloat pixelHeight = [(__bridge NSNumber *)pixelHeightRef floatValue];
            CGFloat maxEdge = MAX(pixelWidth, pixelHeight);
            
            float maxEdgeSize = MAX(size.width, size.height);
            
            if (maxEdge > maxEdgeSize)
            {
                NSDictionary *thumbnailOptions = [NSDictionary dictionaryWithObjectsAndKeys:(id)kCFBooleanTrue,
                                                  kCGImageSourceCreateThumbnailWithTransform, kCFBooleanTrue,
                                                  kCGImageSourceCreateThumbnailFromImageAlways, [NSNumber numberWithFloat:maxEdgeSize],
                                                  kCGImageSourceThumbnailMaxPixelSize, kCFBooleanFalse, kCGImageSourceShouldCache, nil];
                
                imageRef = CGImageSourceCreateThumbnailAtIndex(imageSource, 0, (__bridge CFDictionaryRef)thumbnailOptions);
            }
            else
            {
                NSDictionary *thumbnailOptions = [NSDictionary dictionaryWithObjectsAndKeys:(id)kCFBooleanTrue,
                                                  kCGImageSourceCreateThumbnailWithTransform, kCFBooleanTrue,
                                                  kCGImageSourceCreateThumbnailFromImageAlways, kCFBooleanFalse, kCGImageSourceShouldCache, nil];
                
                imageRef = CGImageSourceCreateImageAtIndex(imageSource, 0, (__bridge CFDictionaryRef)(thumbnailOptions));
            }
            
            CFRelease(imageProperties);
        }
        else
        {
            NSLog(@"Could not get image properties for '%@'", url);
        }
        
        CFRelease(imageSource);
    }
    else
    {
        NSLog(@"Could not create image src for '%@'", url);
    }
    
    return imageRef;
}

@end
