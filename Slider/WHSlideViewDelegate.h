//
//  WHSlideViewDelegate.h
//  Slider
//
//  Created by James Dumay on 28/01/2014.
//  Copyright (c) 2014 Whimsy. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <QuartzCore/QuartzCore.h>

@class WHSlideView;

@protocol WHSlideViewDelegate <NSObject>

/**
 Number of images available to the slide view
 */
-(NSUInteger)numberOfImagesForSlideView:(WHSlideView*)slideView;

/**
 Get image at index. Implementation MUST be thread safe as image loading is async
 */
-(void)slideView:(WHSlideView*)slideView prepareLayer:(CALayer*)layer index:(NSUInteger)index;

@optional

/**
 Padding distance between CALayers
 */
-(CGFloat)paddingForSlideViewLayers:(WHSlideView*)slideView;

/**
 Index used when slide view is initialised
 */
-(NSUInteger)startingIndexForSlideView:(WHSlideView*)slideView;

/**
 Return a new layer without content, doing as little work as possible.
 If not implemented, this view will construct a new CALayer for you.
 */
-(CALayer*)slideView:(WHSlideView*)slideView newLayerForIndex:(NSUInteger)index;

/**
 Prepares the layer before display
 */
-(void)slideView:(WHSlideView*)slideView layoutChangedForLayer:(CALayer*)layer;

/**
 Called when the layer is clicked
 */
-(void)slideView:(WHSlideView*)slideView layerClicked:(CALayer*)layer;

/**
 Mouse moved event passed from the WHSlideView
 */
-(void)slideView:(WHSlideView*)slideView mouseMovedInLayer:(CALayer*)layer point:(NSPoint)point;

/**
 Called when layer is hidden and provides its current index.
 */
-(void)slideView:(WHSlideView*)slideView didHideLayer:(CALayer*)layer atIndex:(NSUInteger)index;

/**
 Called when layer is visible and provides its current index.
 */
-(void)slideView:(WHSlideView*)slideView didShowLayer:(CALayer*)layer atIndex:(NSUInteger)index;

@end
