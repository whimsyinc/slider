//
//  WHAppDelegate.h
//  Slider
//
//  Created by James Dumay on 28/01/2014.
//  Copyright (c) 2014 Whimsy. All rights reserved.
//

#import <Cocoa/Cocoa.h>

#import "WHSlideView.h"
#import "WHSlideViewDelegate.h"

@interface WHAppDelegate : NSObject <NSApplicationDelegate, WHSlideViewDelegate>

@property (strong) NSMutableArray *items;
@property (assign) IBOutlet NSWindow *window;
@property (weak) IBOutlet WHSlideView *slideView;

- (IBAction)left:(id)sender;
- (IBAction)right:(id)sender;

@end
