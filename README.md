## Slider

Alternative to NSPageController based on CALayers. Slide horizontally between layers using the trackpad or clicking on controls edges.

## Features
* 100% Core Animation with a NSView to host layers.
* Layer preparation occurs in background (no lag when loading images or other content)
* Makes extensive use of the delegate to customise and control behavior.
* 10.7 and up (tested only on 10.9 but should be API compatible)

## Example
![](https://bitbucket.org/whimsyinc/slider/raw/8ca39bc8ad7ed125ea0c392d077160d8750d2ffa/demo.gif)

## License

The MIT License (MIT)

Copyright (c) 2014 James Dumay <jameswdumay@gmail.com>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
